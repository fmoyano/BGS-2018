setup <- list(
  runinfo = "Description of this run",
  savetxt = "sens_6b", # this apends to output file

  # -------- Model options ----------
  dec_fun   = "MM" , # One of: 'MM', '2nd', '1st'
  upt_fun   = "2nd" , # One of: 'MM', '2nd', '1st'

  # -------- Parameter options ----------
  # csv file with default parameters
  pars.default.file = "parsets/pars_6b_all.csv"
)
