#### Model.R ================================================================

#### Documentation ==========================================================
# Main function running the model.
# This version is for use with deSolve ode function. It calculates and returns
# the state variable rates of change.
# author(s):
# Fernando Moyano (fmoyano #at# uni-goettingen.de)
#### ========================================================================

Model_desolve <- function(t, initial_state, pars) {
  # must be defined as: func <- function(t, y, parms,...) for use with ode

  with(as.list(c(initial_state, pars)), {

    # set time used for interpolating input data.
    t_i <- t
    # if (spinup) t_i <- t%%end  # this causes spinups to repeat the input data

    # Calculate the input and forcing at time t
    I_sl <- Approx_I_sl(t_i)
    I_ml <- Approx_I_ml(t_i)
    temp <- Approx_temp(t_i)
    moist <- Approx_moist(t_i)

    # Calculate temporally changing variables
    K_D   <- TempRespEq(K_D_ref, temp, T_ref, E_K, R)
    if(upt_fun == "MM") K_U   <- TempRespEq(K_U_ref, temp, T_ref, E_K, R)
    V_D   <- TempRespEq(V_D_ref, temp, T_ref, E_V, R)
    V_U <- TempRespEq(V_U_ref, temp, T_ref, E_V, R)
    r_md  <- TempRespEq(r_md_ref, temp, T_ref, E_m , R)
    r_ed  <- TempRespEq(r_ed_ref, temp, T_ref, E_e , R)
    r_mr  <- TempRespEq(r_mr_ref, temp, T_ref, E_r , R)
    
    ## Diffusion calculations  --------------------------------------
    g_sm <- get_g_sm(moist, ps, Dth, n, p2) # Diffusion modifiers for soil (texture): g_sm
    g <- g_0 * g_sm

    C_D_dif <- g * C_D

    ### Calculate all fluxes ------
    # Input rate
    F_slcp <- I_sl
    F_mlcd <- I_ml

    # Decomposition rate
    if(dec_fun == "MM") {
      F_cpcd <- ReactionMM(C_P, C_E, V_D, K_D, depth)
    }
    if(dec_fun == "2nd") {
      F_cpcd <- Reaction2nd(C_P, C_E, V_D, depth)
    }
    if(dec_fun == "1st") {
      F_cpcd <- Reaction1st(C_P, V_D)
    }

    # Calculate the uptake flux
    if(upt_fun == "MM") {
      Ucd <- ReactionMM(C_D_dif, C_M, V_U, K_U, depth)
    }
    if(upt_fun == "2nd") {
      Ucd <- Reaction2nd(C_D_dif, C_M, V_U, depth)
    }
    if(upt_fun == "1st") {
      Ucd <- Reaction1st(C_D_dif, V_U)
    }

    # Microbial growth, mortality, respiration and enzyme production
    F_cdcm <- Ucd * f_ug * (1 - f_ge)
    F_cdcr <- Ucd * (1 - f_ug)
    F_cdem <- Ucd * f_ug * f_ge
    
    F_cmcp <- C_M * r_md
    F_cmcr <- C_M * r_mr
    
    F_emed <- g * (C_Em - C_E)
    F_emcd <- C_Em * r_ed
    F_edcd <- C_E * r_ed

    ## Rate of change calculation for state variables ---------------
    dC_P <- F_slcp + F_cmcp - F_cpcd
    dC_D <- F_mlcd + F_cpcd + F_edcd + F_emcd - F_cdcm - F_cdcr - F_cdem
    dC_E <- F_emed - F_edcd
    dC_Em <- F_cdem - F_emed - F_emcd
    dC_M <- F_cdcm - F_cmcp - F_cmcr
    dC_Rg <- F_cdcr
    dC_Rm <- F_cmcr

    return(list(c(dC_P, dC_D, dC_E, dC_Em, dC_M, dC_Rg, dC_Rm), C_dec_r = F_cpcd))

  })  # end of with(...

}  # end of Model
