### ===================================== ###
### Post-process at end of run ###
### ===================================== ###

require(deSolve)
require(FME)
library(doParallel)

source("GetModelData.R")
source("InitialState.R")
source("PrepInputCommon.R")

cores = detectCores()
# cores = 1
cat("Cores detected:", cores, "\n")
registerDoParallel(cores = cores)

# path <-'/home/fernando/Desktop/TempMoistExp_maize/Model_tag_what' 
# file <- ''
# 
# load(file.path(path, file))

## ------------------------------------ ##
##  Check parameter sensitivities     ----
## ------------------------------------ ##

# par_corr_plot <- pairs(Sfun, which = c("C_R"), col = c("blue", "green"))
# ident <- collin(Sfun)
# ident_plot <- plot(ident, ylim=c(0,20))
# ident[ident$N==9 & ident$collinearity<15,]

## ------------------------------------ ##
### -------- Get model data ----------- ##           
## ------------------------------------ ##

# # pars_replace <- mcmcMod$bestpar
pars_replace <- fitMod$par
pars_rep <- ParsReplace(pars_replace, pars_default)
pars <- PrepInputCommon(pars_rep)

# Calculate the initial state
initial_state <<- InitialState(pars)

# Get model output with optimized parameters
system.time(mod.out <- GetModelData(pars))

## ------------------------------------ ##
### -------- Post-process data -------- ##           
## ------------------------------------ ##

source("AccumCalc.R")

# Get accumulated values to match observations and merge datasets
data.accum <- merge(obs.accum, AccumCalc(mod.out, obs.accum),
                    by.x = c("treatment", "hour"), by.y = c("treatment", "time"))
# get hourly rates [gC kg-1 h-1]
data.accum$C_R_rm <- data.accum$C_R_m / data.accum$time_accum
data.accum$C_Rm_r <- data.accum$C_Rm / data.accum$time_accum
data.accum$C_Rg_r <- data.accum$C_Rg / data.accum$time_accum
data.accum$C_dec_r <- data.accum$C_dec / data.accum$time_accum
data.accum$C_R_ro <- data.accum$C_R_r # Observed data already gC kg-1 h-1
data.accum$C_R <- NULL

dta <- data.accum
# Convert to mg kg-1 h-1
dta$C_R_ro <- data.accum$C_R_ro * 1000
dta$C_R_rm <- data.accum$C_R_rm * 1000
dta$C_Rm_r <- data.accum$C_Rm_r * 1000
dta$C_Rg_r <- data.accum$C_Rg_r * 1000
dta$C_dec_r <- data.accum$C_dec_r * 1000

# create a group variable
dta$temp_group <- as.factor(dta$temp)

# create a group variable
dta$moist_group <- as.factor(dta$moist_vol)

source("post_process_FitTemp.R")
fit.temp <- FitTemp(dta)
