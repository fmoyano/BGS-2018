### Plot together results from different models
library(plyr)
library(reshape2)
library(ggplot2)
library(RColorBrewer)
library(ggthemes)

rm(list = ls())

DPlot <- function(dta, name, model) {
  dplot <- subset(
    dta, select = c(moist_vol, C_R_ro, C_R_rm, temp, temp_group))
  dplot <- melt(data = dplot, measure.vars = c('C_R_ro', 'C_R_rm'))
  dplot$flux <- revalue(
    dplot$variable, c('C_R_ro'='obs', 'C_R_rm'= paste(model, name, sep = "-")))
  dplot$flux <- as.character(dplot$flux)
  return(dplot)
}

load("data_psi-decMM-upt2nd-diffhama-errorC_R_sd01.RData")
dplot <- DPlot(dta, "wp", "M2")

load("data_sat-decMM-upt2nd-diffhama-errorC_R_sd01.RData")
dplot <- rbind(dplot[dplot$flux!='obs',], DPlot(dta, "sat", "M2"))

load("data_dif-decMM-upt2nd-diffhama-errorC_R_sd01.RData")
dplot <- rbind(dplot[dplot$flux!='obs',], DPlot(dta, "dif", "M2"))

dplot$Flux <- factor(dplot$flux, levels = c("obs", "M2-dif", "M2-wp", "M2-sat"))

################################################################################
## Plots
################################################################################

prefix <- "plot_"
savedir <- file.path("plots")
devname <- "pdf"
devfun <- pdf
plotname <- paste0(prefix, "moist-resp.", devname)
plotfile <- file.path(savedir, plotname)
export <- 1

# ------------------------------------------------------------------------------
# Plot mod and obs flux vs moisture values for each temp group -----------------
# ------------------------------------------------------------------------------

col2 <- brewer.pal(8, "Accent")[c(8,6,1,5)]
col3 <- c(brewer.pal(8, "Accent")[c(5,1)], '#555555', '#999999')
col4 <- c("#104E8B77", 'grey70', 'grey50', 'grey30')
col5 <- c("grey20", 'grey60', 'grey40', 'grey80')

labs <- data.frame(temp_group = c("5", "20", "35"), 
                   label = c("5 °C", "20 °C", "35 °C"),
                   Flux = rep("obs", 3))

if(export) devfun(file = plotfile, width = 4, height = 8) #width = 700, height = 1000

p <- ggplot(data = dplot, aes(x = moist_vol, y = value, colour = Flux,
                               shape = Flux)) +
  geom_smooth(se = FALSE, size = 1.2) + #, aes(linetype= flux)) +
  geom_point(size = 1.3) +
  scale_color_manual(values = col3) +
  xlab(expression(paste("Soil moisture (", m^3*m^-3,")"))) +
  ylab(expression(paste("Respired ", CO[2], " (",mg~C~kg^-1*soil~h^-1, ")"))) +
  scale_y_continuous(limits = c(0, NA)) +
  theme_bw(base_size = 12) +
  # scale_color_manual(values = darkcols) +
  theme(axis.line = element_line(colour = "black"),
        # legend.direction = "horizontal",
        legend.position = c(0.17,0.55), 
        legend.title = element_blank(),
        # legend.key.height=unit(0.5,'cm'), legend.key.width=unit(1,'cm'),
        legend.background = element_rect(fill="transparent"),
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        strip.background = element_blank(),
        strip.text.y = element_blank()
  ) +
  # facet_wrap("temp_group", scales = "free_y")
  facet_grid(temp_group~., scales = "free") +
  geom_text(data = labs, size = 4, color = "grey10",
                    mapping = aes(x = -Inf, y = -Inf, label = label),
                    hjust   = -0.2,
                    vjust   = -20
            )

print(p)

if(export) while(dev.cur() > 1) dev.off()
