library(ggplot2)

# Random plotting
out <- as.data.frame(mod.out)
out$VWC <- out$moist
# Converting to mgC (note that optimization code already converts to gC kg-1 h-1)
out_raw <- out
# out[,c(2:8, 12:13)] <- out_raw[,c(2:8, 12:13)] * 1000

# Correlation decomposed vs respired
pal1 <- topo.colors # c('#9ACD32', '#698B22', '#CD8500') # 'olivedrab3', 'olivedrab4', 'orange3'
pal2 <- c('#E9967A', '#1E90FF')
pal3 <- palette("gray30", "gray90")

prefix <- "plot_"
savedir <- file.path("plots")
devname <- "pdf"
devfun <- pdf
export <- 1
plotname <- paste0(prefix, "dec-resp.", devname)
plotfile <- file.path(savedir, plotname)

if(export) devfun(file = plotfile, width = 4.5, height = 4) # width = 600, height = 930)
ggplot(data = out, aes(x=C_dec, y=C_R, group=treatment, colour=VWC)) +
  scale_colour_gradientn(colours = pal2) + # terrain.colors
  geom_line(size = 1.5) +
  ylab(expression(paste("Respired C (", g~C~kg^-1*soil,")"))) +
  xlab(expression(paste("Decomposed C (", g~C~kg^-1*soil,")"))) +
  theme_bw(base_size = 12) +
  theme(legend.justification=c(1,1), legend.position=c(0.2,0.97),
        legend.box='horizontal',
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank())
dev.off()
