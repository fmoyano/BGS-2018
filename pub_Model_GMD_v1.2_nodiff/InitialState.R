###  Calculate initial C pool sizes
InitialState <- function(pars) {
  TOC <- pars[['toc']] * pars[["pd"]] * (1 - pars[["ps"]]) * pars[["depth"]]
  C_P <- TOC * (1 - pars[['f_CD']] - pars[['f_CE']] * 2 - pars[['f_CM']])
  C_D <- TOC * pars[['f_CD']]
  C_E <- TOC * pars[['f_CE']]
  C_M <- TOC * pars[['f_CM']]
  C_Rg <- 0
  C_Rm <- 0
  initial_state <- c(C_P = C_P, C_D = C_D, C_E = C_E, C_M = C_M, C_Rg = C_Rg, C_Rm = C_Rm)
}
