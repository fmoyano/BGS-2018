# Plot treatments
# requires results from process_data.r

library(ggplot2)
library(RColorBrewer)

plotdat <- read.csv("mtdata_model_input.csv")

plotdat$degC <- as.factor(plotdat$temp - 273)

p1 <- ggplot(data = plotdat, mapping = aes(x = hour, y = moist)) +
  theme_classic(base_size = 10) +
  theme(
        panel.grid.major = element_blank(),
        panel.grid.minor = element_blank(),
        panel.background = element_blank(),
        legend.box = "horizontal",
        legend.position = "top"
        ) +
  geom_line(aes(group = treatment, color = degC), size = 1.5) +
  scale_color_manual(values=c("gray80", "grey60", "grey30")) +
  ylab(expression(paste("Soil moisture (", m^3*m^-3,")"))) +
  xlab('Incubation Hours') +
  labs(color = expression(degree*C))

p1
